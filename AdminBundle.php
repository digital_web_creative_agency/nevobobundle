<?php

namespace DigitalWeb\AlbumBundle;

use DigitalWeb\AlbumBundle\DependencyInjection\AlbumExtension;
use Sulu\Component\Content\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class AlbumBundle extends AbstractBundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new AlbumExtension();
    }
}
